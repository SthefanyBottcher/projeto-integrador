<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:import url="/Utils/ModalComfirma.jsp"></c:import>
<c:import url="/Utils/ModalError.jsp"></c:import>
<c:import url="/Utils/ModalComfirmaAlter.jsp"></c:import>
<c:import url="/Utils/ModalSucesso.jsp"></c:import>

<script>

    function ConfimarCliente(CPF) {
        $("#modalConfimar").show();
        var html = 'Confirmar Exclus�o do cliente '+ CPF +' ?';
        $("#Frase").html(html);
        $("#Tipo").val('CLIENTE');
        $("#Valor").val(CPF);
    }
    function Deletar() {
          var valor = $("#Valor").val();
        if ( $("#Tipo").val()== 'CLIENTE') {
            $("#modalConfimar").hide();
            $.ajax({
                method: "GET",
                url: "ExcluirCliente?cpf=" + valor
            }).done(function (msg) {
                window.location.reload();
            });
        } else if ( $("#Tipo").val() == 'PRODUTO') {
            $("#modalConfimar").hide();
            
            $.ajax({
                method: "GET",
                url: "ProdutoExcluir?cod_Produto=" + valor
            }).done(function (msg) {
                window.location.reload();
            });
        }
    }
    function Close(id) {
        $("#" + id + "").hide();
    }
    function UpCliente() {
        $("#modalConfimar").hide();
        $("#FormAlte").submit();
    }
    function ConfimarProduto(idProduto) {
        $("#modalConfimar").show();
        var html = 'Confirmar Exclus�o do Produto ' +idProduto+' ?';
        $("#Frase").html(html);
        $("#Tipo").val('PRODUTO');
        $("#Valor").val(idProduto);
    }
    $(function () {
        $("#BntAlter").click(function () {
            $("#modalConfimaalter").show();
            var cpf = $("#CPF").val();
            $("#CPFCLIENTE").html(cpf)
        });
    });
</script>
