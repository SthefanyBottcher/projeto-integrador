<div class="modal" id="modalErro" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Erro</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" onclick="Close('modalErro')"></button>
      </div>
      <div class="modal-body">
        <p>Aten��o solicita��o n�o pode ser concluida tentar novamente </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" onclick="Close('modalErro')" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>