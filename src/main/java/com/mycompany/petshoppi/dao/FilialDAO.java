/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshoppi.dao;
 
import com.mycompany.petshoppi.Entidade.Filial;
import com.mycompany.petshoppi.util.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author joaop
 */
public class FilialDAO 
{
   static Connection conexao;
    private static final String DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    static String sql;
    
    public FilialDAO() {}
    
    public static boolean CadastrarEstabelecimento(Filial model)
    {
        boolean status = false;
        
        try 
        {
            Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            sql = "insert into Filial_PetShop (Gerente_Filial,Filial_loc, cep, numero) values (?,?,?)";

            PreparedStatement instrucaoSQL = conexao.prepareStatement(sql);
            
            instrucaoSQL.setString(1, model.getGerente_Filial());
            instrucaoSQL.setString(2, model.getFilial_loc());
            instrucaoSQL.setString(3, model.getCEP());
            instrucaoSQL.setInt(4, model.getNumero());

            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) 
                status = true;
             
            else
              throw new Exception();
           
            conexao.close();
        } 
        catch (Exception e) 
        {
          System.out.println("Erro ao iniciar conexão com o BD");
        }
        
        return status;
    }
    
    public static boolean AlterarFilial(Filial model)
    {
        boolean status = false;
        
        try 
        {
            Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            sql = "update Filial_PetShop set Gerente_Filial = ? where cod_Filial = ?";

            PreparedStatement instrucaoSQL = conexao.prepareStatement(sql);
            
            instrucaoSQL.setString(1, model.getGerente_Filial());           
            instrucaoSQL.setInt(4, model.getCod_Filial());
            
            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) 
                status = true;

             else 
               throw new Exception();

            conexao.close();
        } 
        catch (Exception e) 
        {
           System.out.println("Erro ao iniciar conexão com o BD");
        }
        
        return status;
    }
    
    public static boolean RemoverEstabelecimento(Filial model)
    {
        boolean status = false;
        
        try 
        {
            Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            sql = "delete from Filial_PetShop where cod_Filial = ?";

            PreparedStatement instrucaoSQL = conexao.prepareStatement(sql);
            
            instrucaoSQL.setInt(1, model.getCod_Filial());
            
            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) 
                status = true;

            else 
               throw new Exception();
            
            conexao.close();
        } 
        catch (Exception e)
        {
           System.out.println("Erro ao iniciar conexão com o BD");
        }
        
        return status;
    }
    
    public static boolean ValidarEstabelecimento(Filial model)
    {
        boolean status = false;
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;
        
        try 
        {
            Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            sql = "select * from Filial_PetShop where cod_Filial = ?";

            instrucaoSQL = conexao.prepareStatement(sql);
            instrucaoSQL.setInt(1, model.getCod_Filial());
            
            rs = instrucaoSQL.executeQuery();
    
            if (rs != null) 
            {
                rs.close();
                System.out.println("Estabelecimento existe!");
            }

            if (instrucaoSQL != null) {
                instrucaoSQL.close();
                System.out.println("Estabelecimento não existe.");
            }
            
            conexao.close();
            
        } 
        catch (Exception e) 
        {
            System.out.println("Erro na consulta.");
        }
        
        return status;
        
    }
    
    public static Filial ConsultarEstabelecimento(Filial model)
    {
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;
    
        Filial fila = new Filial();
        try 
        {
            Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            sql = "select * from Filial_PetShop where cod_Filial = ?";
            
            instrucaoSQL = conexao.prepareStatement(sql);
            instrucaoSQL.setInt(1, model.getCod_Filial());
            
            rs = instrucaoSQL.executeQuery();
            
            if (rs.next()) 
            {
                 fila.setCod_Filial(rs.getInt("cod_Filial"));
                 fila.setGerente_Filial(rs.getString("Gerente_Filial"));
                  fila.setFilial_loc(rs.getString("Filial_loc"));
                 fila.setCEP(rs.getString("CEP"));
                 fila.setNumero(rs.getInt("Numero"));
            }
            
        } 
        catch (Exception e) 
        {
            System.out.println("Erro na consulta");
        } 
        
        finally 
        {
            try 
            {
                if (rs != null) 
                {
                    rs.close();
                    System.out.println("Estabelecimento existe! Fim");
                }

                if (instrucaoSQL != null) 
                {
                    instrucaoSQL.close();
                    System.out.println("Estabelecimento não existe. Fim");
                }

                conexao.close();
            } 
            catch (Exception e) 
            {
                System.out.println("Falha no fechamento da conexão");
            }
            
        }
        
        return fila;
    }    
    
    public static ArrayList<Filial> ListarEstabelecimentos(Filial model)
    {
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;

        ArrayList<Filial> listaEstabelecimento = new ArrayList<Filial>();
        
        try 
        {
            Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            sql = "select * from Filial_PetShop";

            instrucaoSQL = conexao.prepareStatement(sql);
            rs = instrucaoSQL.executeQuery();
            
            while (rs.next()) 
            {
                Filial estab = new Filial();
                
                estab.setCod_Filial(rs.getInt("cod_Filial"));
                estab.setGerente_Filial(rs.getString("Gerente_Filial"));
                estab.setFilial_loc(rs.getString("Filial_loc"));
                estab.setNumero(rs.getInt("numero"));
                estab.setCEP(rs.getString("cep"));
                
                listaEstabelecimento.add(estab);
            }
        } 
        catch (Exception e) 
        {
            System.out.println("Erro na listagem");
        } 
        
        finally 
        {
            try 
            {
                if (rs != null) 
                   rs.close();

                if (instrucaoSQL != null) 
                    instrucaoSQL.close();
                
                conexao.close();
            } 
            catch (Exception e) 
            {
                System.out.println("Falha no fechamento da conexão");
            }
        }
        
        return listaEstabelecimento;
    }
    
    public static int PegarId(Filial model)
    {
        int cod_Petshop = 0;
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;
        
        try 
        {
            Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            sql = "select cod_Filial from Filial_PetShop where cep = ?";
            
            instrucaoSQL = conexao.prepareStatement(sql);
            instrucaoSQL.setString(1, model.getCEP());
            
            rs = instrucaoSQL.executeQuery();
            
            while (rs.next()) 
            {
               cod_Petshop = rs.getInt("cod_petshop");
            }
        } 
        catch (Exception e) 
        {
            System.out.println("Erro na consulta");
        } 
        finally 
        {
            try 
            {
                if (rs != null) 
                    rs.close();

                if (instrucaoSQL != null)
                    instrucaoSQL.close();

                conexao.close();

            } 
            catch (Exception e) 
            {
                System.out.println("Falha no fechamento da conexão");
            }
        }
        
        return cod_Petshop;
    }
}
