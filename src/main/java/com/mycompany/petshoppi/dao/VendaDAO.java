/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshoppi.dao;

import com.mycompany.petshoppi.Entidade.Venda;
import com.mycompany.petshoppi.Entidade.VendaItem;
import com.mycompany.petshoppi.Entidade.Filial;
import com.mycompany.petshoppi.util.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author dilaz
 */
public class VendaDAO {

    static Connection conexao;

    /**
     * Driver do MySQL a partir da versão 8.0
     */
    private static final String DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";

    public VendaDAO() {
    }

    public static boolean cadastrarVenda(Venda venda) {

        boolean status = false;

        try {

            //Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            String sql = "insert into Venda (cod_venda, cod_Funcionario, cod_Cliente, cod_Produto, data_atendimento,"
                    + " hora_atendimento,status_venda, tipo_entrega,total_venda,cod_filial) "
                    + "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            PreparedStatement instrucaoSQL = conexao.prepareStatement(sql);

            instrucaoSQL.setInt(1, venda.getCod_venda());
            instrucaoSQL.setInt(2, venda.getCod_Funcionario());
            instrucaoSQL.setInt(3, venda.getCod_Cliente());
            instrucaoSQL.setInt(4, venda.getCod_Produto());
            instrucaoSQL.setString(5, venda.getData_atendimento());
            instrucaoSQL.setString(6, venda.getHora_atendimento());
            instrucaoSQL.setString(7, venda.getStatus_venda());
            instrucaoSQL.setString(8, venda.getTipo_entrega());
            instrucaoSQL.setDouble(9, venda.getTotal_venda());
            instrucaoSQL.setString(10, venda.getCod_filial());

            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                status = true;

            } else {
                throw new Exception();

            }

        } catch (Exception e) {
            System.out.println("Erro ao iniciar conexão com o BD");

        }

        return status;

    }

    public static boolean cadastrarVendaItem(VendaItem venda) {

        boolean status = false;

        try {

            //Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            String sql = "insert into Venda (cod_venda, cod_Funcionario, cod_Cliente, cod_Produto, total_venda) "
                    + "values (?, ?, ?, ?, ?)";

            PreparedStatement instrucaoSQL = conexao.prepareStatement(sql);

            instrucaoSQL.setInt(1, venda.getCod_venda());
            instrucaoSQL.setInt(2, venda.getCod_Funcionario());
            instrucaoSQL.setInt(3, venda.getCod_Cliente());
            instrucaoSQL.setInt(4, venda.getCod_Produto());
            instrucaoSQL.setDouble(5, venda.getTotal_venda());

            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                status = true;

            } else {
                throw new Exception();

            }

        } catch (Exception e) {
            System.out.println("Erro ao iniciar conexão com o BD");

        }

        return status;

    }

    public static boolean alterarVenda(Venda venda) {

        boolean status = false;

        try {

            //Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            String sql = "UPDATE Venda SET cod_venda=?, cod_Funcionario=?, cod_Cliente=?, cod_Produto=?, data_atendimento=?,"
                    + " hora_atendimento=?,status_venda=?, tipo_entrega=?,total_venda=?,cod_filial=?";

            PreparedStatement instrucaoSQL = conexao.prepareStatement(sql);

            instrucaoSQL.setInt(1, venda.getCod_venda());
            instrucaoSQL.setInt(2, venda.getCod_Funcionario());
            instrucaoSQL.setInt(3, venda.getCod_Cliente());
            instrucaoSQL.setInt(4, venda.getCod_Produto());
            instrucaoSQL.setString(5, venda.getData_atendimento());
            instrucaoSQL.setString(6, venda.getHora_atendimento());
            instrucaoSQL.setString(7, venda.getStatus_venda());
            instrucaoSQL.setString(8, venda.getTipo_entrega());
            instrucaoSQL.setDouble(9, venda.getTotal_venda());
            instrucaoSQL.setString(10, venda.getCod_filial());

            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                status = true;

            } else {
                throw new Exception();

            }

            conexao.close();

        } catch (Exception e) {
            System.out.println("Erro ao iniciar conexão com o BD");

        }

        return status;

    }

    public static boolean alterarVendaItem(VendaItem venda) {

        boolean status = false;

        try {

            //Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            String sql = "UPDATE Venda SET cod_venda=?, cod_Funcionario=?, cod_Cliente=?, cod_Produto=?"
                    + " total_venda=?";

            PreparedStatement instrucaoSQL = conexao.prepareStatement(sql);

            instrucaoSQL.setInt(1, venda.getCod_venda());
            instrucaoSQL.setInt(2, venda.getCod_Funcionario());
            instrucaoSQL.setInt(3, venda.getCod_Cliente());
            instrucaoSQL.setInt(4, venda.getCod_Produto());
            instrucaoSQL.setDouble(5, venda.getTotal_venda());

            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                status = true;

            } else {
                throw new Exception();

            }

            conexao.close();

        } catch (Exception e) {
            System.out.println("Erro ao iniciar conexão com o BD");

        }

        return status;

    }

    public static boolean removerVenda(int venda) {

        boolean status = false;

        try {

            //Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            String sql = "delete from Venda where cod_venda = ?";

            PreparedStatement instrucaoSQL = conexao.prepareStatement(sql);

            instrucaoSQL.setInt(1, venda);

            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                status = true;

            } else {
                throw new Exception();

            }

        } catch (Exception e) {
            System.out.println("Erro ao iniciar conexão com o BD");

        }

        return status;

    }

    public static boolean removerVendaItem(int venda) {

        boolean status = false;

        try {

            //Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            String sql = "delete from Venda where cod_venda = ?";

            PreparedStatement instrucaoSQL = conexao.prepareStatement(sql);

            instrucaoSQL.setInt(1, venda);

            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                status = true;

            } else {
                throw new Exception();

            }

        } catch (Exception e) {
            System.out.println("Erro ao iniciar conexão com o BD");

        }

        return status;

    }

    public static boolean validarVenda(Venda venda) {

        boolean status = false;
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;

        try {

            //Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            String sql = "select * from Venda where cod_venda = ?";

            instrucaoSQL = conexao.prepareStatement(sql);

            instrucaoSQL.setInt(1, venda.getCod_venda());

            rs = instrucaoSQL.executeQuery();

            if (rs != null) {
                rs.close();
                System.out.println("Venda existe!");

            }

            if (instrucaoSQL != null) {
                instrucaoSQL.close();
                System.out.println("Venda não existe.");

            }

            conexao.close();

        } catch (Exception e) {
            System.out.println("Erro na consulta.");

        }

        return status;

    }

    public static boolean validarVendaItem(VendaItem venda) {

        boolean status = false;
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;

        try {

            //Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            String sql = "select * from Venda where cod_venda = ?";

            instrucaoSQL = conexao.prepareStatement(sql);

            instrucaoSQL.setInt(1, venda.getCod_venda());

            rs = instrucaoSQL.executeQuery();

            if (rs != null) {
                rs.close();
                System.out.println("Venda existe!");

            }

            if (instrucaoSQL != null) {
                instrucaoSQL.close();
                System.out.println("Venda não existe.");

            }

            conexao.close();

        } catch (Exception e) {
            System.out.println("Erro na consulta.");

        }

        return status;

    }

    public static Venda consultarVenda(int venda) {

        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;
        Venda ve = new Venda();
        try {

            // Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            String sql = "select * from Venda where cod_venda = ?";

            instrucaoSQL = conexao.prepareStatement(sql);

            instrucaoSQL.setInt(1, venda);

            rs = instrucaoSQL.executeQuery();

            if (rs.next()) {

                ve.setCod_venda(rs.getInt("cod_venda"));
                ve.setCod_Funcionario(rs.getInt("cod_Funcionario"));
                ve.setCod_Cliente(rs.getInt("cod_Cliente"));
                ve.setCod_Produto(rs.getInt("cod_Produto"));
                ve.setData_atendimento(rs.getString("data_atendimento"));
                ve.setHora_atendimento(rs.getString("hora_atendimento"));
                ve.setStatus_venda(rs.getString("status_venda"));
                ve.setTipo_entrega(rs.getString("tipo_entrega"));
                ve.setTotal_venda(rs.getDouble("total_venda"));
                ve.setCod_filial(rs.getString("cod_filial"));
            }

        } catch (Exception e) {
            System.out.println("Erro na consulta");

        } finally {

            try {

                if (rs != null) {
                    rs.close();
                    System.out.println("Venda existe! Fim");

                }

                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                    System.out.println("Venda não existe. Fim");

                }

                conexao.close();

            } catch (Exception e) {
                System.out.println("Falha no fechamento da conexão");

            }

        }

        return ve;

    }

    public static Venda consultarVendaItem(int venda) {

        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;
        Venda ve = new Venda();
        try {

            // Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            String sql = "select * from Venda where cod_venda = ?";

            instrucaoSQL = conexao.prepareStatement(sql);

            instrucaoSQL.setInt(1, venda);

            rs = instrucaoSQL.executeQuery();

            if (rs.next()) {

                ve.setCod_venda(rs.getInt("cod_venda"));
                ve.setCod_Funcionario(rs.getInt("cod_Funcionario"));
                ve.setCod_Cliente(rs.getInt("cod_Cliente"));
                ve.setCod_Produto(rs.getInt("cod_Produto"));
                ve.setTotal_venda(rs.getDouble("total_venda"));
            }

        } catch (Exception e) {
            System.out.println("Erro na consulta");

        } finally {

            try {

                if (rs != null) {
                    rs.close();
                    System.out.println("Venda existe! Fim");

                }

                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                    System.out.println("Venda não existe. Fim");

                }

                conexao.close();

            } catch (Exception e) {
                System.out.println("Falha no fechamento da conexão");

            }

        }

        return ve;
    }

    public static ArrayList<Venda> listarVenda() {

        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;

        ArrayList<Venda> listaVenda = new ArrayList<Venda>();

        try {

            // Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            String sql = "select * from Venda";

            instrucaoSQL = conexao.prepareStatement(sql);

            rs = instrucaoSQL.executeQuery();

            while (rs.next()) {

                Venda ve = new Venda();

                ve.setCod_venda(rs.getInt("cod_venda"));
                ve.setCod_Funcionario(rs.getInt("cod_Funcionario"));
                ve.setCod_Cliente(rs.getInt("cod_Cliente"));
                ve.setCod_Produto(rs.getInt("cod_Produto"));
                ve.setData_atendimento(rs.getString("data_atendimento"));
                ve.setHora_atendimento(rs.getString("hora_atendimento"));
                ve.setStatus_venda(rs.getString("status_venda"));
                ve.setTipo_entrega(rs.getString("tipo_entrega"));
                ve.setTotal_venda(rs.getDouble("total_venda"));
                ve.setCod_filial(rs.getString("cod_filial"));
                listaVenda.add(ve);

            }

        } catch (Exception e) {
            System.out.println("Erro na listagem");

        } finally {

            try {

                if (rs != null) {
                    rs.close();

                }

                if (instrucaoSQL != null) {
                    instrucaoSQL.close();

                }

                conexao.close();

            } catch (Exception e) {
                System.out.println("Falha no fechamento da conexão");

            }

        }

        return listaVenda;

    }

    public static ArrayList<VendaItem> listarVendaItem() {

        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;

        ArrayList<VendaItem> listaVendaItem = new ArrayList<VendaItem>();

        try {

            // Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            String sql = "select * from Venda";

            instrucaoSQL = conexao.prepareStatement(sql);

            rs = instrucaoSQL.executeQuery();

            while (rs.next()) {

                VendaItem ve = new VendaItem();

                ve.setCod_venda(rs.getInt("cod_venda"));
                ve.setCod_Funcionario(rs.getInt("cod_Funcionario"));
                ve.setCod_Cliente(rs.getInt("cod_Cliente"));
                ve.setCod_Produto(rs.getInt("cod_Produto"));
                ve.setTotal_venda(rs.getDouble("total_venda"));
                listaVendaItem.add(ve);

            }

        } catch (Exception e) {
            System.out.println("Erro na listagem");

        } finally {

            try {

                if (rs != null) {
                    rs.close();

                }

                if (instrucaoSQL != null) {
                    instrucaoSQL.close();

                }

                conexao.close();

            } catch (Exception e) {
                System.out.println("Falha no fechamento da conexão");

            }

        }

        return listaVendaItem;

    }

    public static int pegarId(Venda venda) {

        int cod_venda = 0;
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;

        try {

            Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            String sql = "select cod_venda from Venda where cod_venda = ?";

            instrucaoSQL = conexao.prepareStatement(sql);

            instrucaoSQL.setInt(1, venda.getCod_venda());

            rs = instrucaoSQL.executeQuery();

            if (rs.next()) {

                cod_venda = rs.getInt("cod_venda");

            }

        } catch (Exception e) {
            System.out.println("Erro na consulta");

        } finally {

            try {

                if (rs != null) {
                    rs.close();

                }

                if (instrucaoSQL != null) {
                    instrucaoSQL.close();

                }

                conexao.close();

            } catch (Exception e) {
                System.out.println("Falha no fechamento da conexão");

            }

        }

        return cod_venda;

    }

        public static int pegarIdItem(VendaItem venda) {

        int cod_venda = 0;
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;

        try {

            Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            String sql = "select cod_venda from Venda where cod_venda = ?";

            instrucaoSQL = conexao.prepareStatement(sql);

            instrucaoSQL.setInt(1, venda.getCod_venda());

            rs = instrucaoSQL.executeQuery();

            if (rs.next()) {

                cod_venda = rs.getInt("cod_venda");

            }

        } catch (Exception e) {
            System.out.println("Erro na consulta");

        } finally {

            try {

                if (rs != null) {
                    rs.close();

                }

                if (instrucaoSQL != null) {
                    instrucaoSQL.close();

                }

                conexao.close();

            } catch (Exception e) {
                System.out.println("Falha no fechamento da conexão");

            }

        }

        return cod_venda;

    }
}
