/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshoppi.dao;

import com.mycompany.petshoppi.Entidade.Produto;
import com.mycompany.petshoppi.util.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author joaop
 */
public class ProdutoDAO {

    static Connection conexao;
    static String sql;

    public ProdutoDAO() {
    }

    public static boolean cadastrarProduto(Produto model) {
        boolean status = false;

        try {
            conexao = Conexao.abrirConexao();

            sql = "insert into Produto (tipo_Animal, nome_Produto, valor_Venda,valor_entrada, status_produto, codigo_barras, descricao, classificacao, "
                    + "data_cadastro, data_entrada, quantidade, fornecedor) values (?,?,?,?,?,?,?,?,?,?,?,?)";

            PreparedStatement instrucaoSQL = conexao.prepareStatement(sql);

            instrucaoSQL.setString(1, model.getTipo_Animal());
            instrucaoSQL.setString(2, model.getNome_Produto());
            instrucaoSQL.setDouble(3, model.getValor_Venda());
            instrucaoSQL.setDouble(4, model.getValor_Entrada());
            instrucaoSQL.setString(5, model.getStatus_produto());
            instrucaoSQL.setString(6, model.getCodigo_barras());
            instrucaoSQL.setString(7, model.getDescricao());
            instrucaoSQL.setString(8, model.getClassificacao());
            instrucaoSQL.setString(9, model.getData_cadastro());
            instrucaoSQL.setString(10, model.getData_entrada());
            instrucaoSQL.setInt(11, model.getQuantidade());
            instrucaoSQL.setString(12, model.getFornecedor());

            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                status = true;
            } else {
                throw new Exception();
            }

            conexao.close();
        } catch (Exception e) {
            System.out.println("Erro ao iniciar conexão com o BD");
        }

        return status;
    }

    public static boolean alterarProduto(Produto model) {
        boolean status = false;

        try {
            conexao = Conexao.abrirConexao();

            sql = "update Produto set tipo_Animal = ?,nome_Produto= ?, valor_Venda = ?,  valor_entrada =?,status_produto = ?, codigo_barras = ?, "
                    + "descricao = ?, classificacao = ?, data_entrada = ?, quantidade = ?, fornecedor = ? "
                    + "where cod_Produto = ?";

            PreparedStatement instrucaoSQL = conexao.prepareStatement(sql);

            
            instrucaoSQL.setString(1, model.getTipo_Animal());
            instrucaoSQL.setString(2, model.getNome_Produto());
            instrucaoSQL.setDouble(3, model.getValor_Venda());
            instrucaoSQL.setDouble(4, model.getValor_Entrada());
            instrucaoSQL.setString(5, model.getStatus_produto());
            instrucaoSQL.setString(6, model.getCodigo_barras());
            instrucaoSQL.setString(7, model.getDescricao());
            instrucaoSQL.setString(8, model.getClassificacao());
            instrucaoSQL.setString(9, model.getData_entrada());
            instrucaoSQL.setInt(10, model.getQuantidade());
            instrucaoSQL.setString(11, model.getFornecedor());
            instrucaoSQL.setInt(12, model.getCod_Produto());

            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                status = true;
            } else {
                throw new Exception();
            }

            conexao.close();
        } catch (Exception e) {
            System.out.println("Erro ao iniciar conexão com o BD");
        }

        return status;
    }

    public static boolean removerProduto(int model) {//alterei
        boolean status = false;

        try {
          
            conexao = Conexao.abrirConexao();

            sql = "delete from Produto where cod_Produto = ?";

            PreparedStatement instrucaoSQL = conexao.prepareStatement(sql);

            instrucaoSQL.setInt(1, model);//alterei
//            instrucaoSQL.setInt(1, model.getCod_Produto());

            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) {
                status = true;
            } else {
                throw new Exception();
            }

            conexao.close();
        } catch (Exception e) {
            System.out.println("Erro ao iniciar conexão com o BD");
        }

        return status;
    }

    public static boolean validarProduto(Produto model) {
        boolean status = false;
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;

        try {
            conexao = Conexao.abrirConexao();

            sql = "select * from Produto where cod_Produto = ?";

            instrucaoSQL = conexao.prepareStatement(sql);
            instrucaoSQL.setInt(1, model.getCod_Produto());

            rs = instrucaoSQL.executeQuery();

            if (rs != null) {
                rs.close();
                System.out.println("Produto existe!");
            }

            if (instrucaoSQL != null) {
                instrucaoSQL.close();
                System.out.println("Produto não existe.");
            }

            conexao.close();

        } catch (Exception e) {
            System.out.println("Erro na consulta.");
        }

        return status;

    }

    public static Produto consultarProduto(int model) { //mudei aqui de Produto pra int
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;
        Produto prod = new Produto();

        try {
            conexao = Conexao.abrirConexao();

            sql = "select * from Produto where cod_Produto = ?";

            instrucaoSQL = conexao.prepareStatement(sql);
            instrucaoSQL.setInt(1, model);//alterei aqui tb.

            rs = instrucaoSQL.executeQuery();

            if (rs.next()) {

                prod.setCod_Produto(rs.getInt("cod_Produto"));
                prod.setNome_Produto(rs.getString("nome_Produto"));
                prod.setTipo_Animal(rs.getString("tipo_Animal"));
                prod.setValor_Venda(rs.getDouble("valor_Venda"));
                prod.setValor_Entrada(rs.getDouble("valor_Entrada"));
                prod.setStatus_produto(rs.getString("status_produto"));
                prod.setCodigo_barras(rs.getString("codigo_barras"));
                prod.setDescricao(rs.getString("descricao"));
                prod.setClassificacao(rs.getString("classificacao"));
                prod.setData_cadastro(rs.getString("data_cadastro"));
                prod.setData_entrada(rs.getString("data_entrada"));
                prod.setQuantidade(rs.getInt("quantidade"));
                prod.setFornecedor(rs.getString("fornecedor"));

            }

        } catch (Exception e) {
            System.out.println("Erro na consulta");
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                    System.out.println("Produto existe! Fim");
                }

                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                    System.out.println("Produto não existe. Fim");
                }

                conexao.close();
            } catch (Exception e) {
                System.out.println("Falha no fechamento da conexão");
            }

        }

        return prod;
    }

    public static ArrayList<Produto> listarProdutos() {
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;

        ArrayList<Produto> listaProduto = new ArrayList<Produto>();

        try {
            conexao = Conexao.abrirConexao();

            sql = "select * from Produto";

            instrucaoSQL = conexao.prepareStatement(sql);
            rs = instrucaoSQL.executeQuery();

            while (rs.next()) {
                Produto prod = new Produto();

                prod.setCod_Produto(rs.getInt("cod_Produto"));
                 prod.setNome_Produto(rs.getString("nome_Produto"));
                prod.setTipo_Animal(rs.getString("tipo_Animal"));
                prod.setValor_Venda(rs.getDouble("valor_Venda"));
                prod.setValor_Entrada(rs.getDouble("valor_Entrada"));
                prod.setStatus_produto(rs.getString("status_produto"));
                prod.setCodigo_barras(rs.getString("codigo_barras"));
                prod.setDescricao(rs.getString("descricao"));
                prod.setClassificacao(rs.getString("classificacao"));
                prod.setData_cadastro(rs.getString("data_cadastro"));
                prod.setData_entrada(rs.getString("data_entrada"));
                prod.setQuantidade(rs.getInt("quantidade"));
                prod.setFornecedor(rs.getString("fornecedor"));

                listaProduto.add(prod);
            }
        } catch (Exception e) {
            System.out.println("Erro na listagem");
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }

                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                }

                conexao.close();
            } catch (Exception e) {
                System.out.println("Falha no fechamento da conexão");
            }
        }

        return listaProduto;
    }

    public static int pegarId(Produto model) {
        int cod_Produto = 0;
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;

        try {
            conexao = Conexao.abrirConexao();

            sql = "select cod_Produto from Produto where codigo_barras = ?";

            instrucaoSQL = conexao.prepareStatement(sql);
            instrucaoSQL.setString(1, model.getCodigo_barras());

            rs = instrucaoSQL.executeQuery();

            while (rs.next()) {
                cod_Produto = rs.getInt("cod_Produto");
            }
        } catch (Exception e) {
            System.out.println("Erro na consulta");
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }

                if (instrucaoSQL != null) {
                    instrucaoSQL.close();
                }

                conexao.close();

            } catch (Exception e) {
                System.out.println("Falha no fechamento da conexão");
            }
        }

        return cod_Produto;
    }
}
