/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshoppi.dao;

import com.mycompany.petshoppi.Entidade.Funcionario;
import com.mycompany.petshoppi.util.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
  
/**
 *
 * @author joaop
 */
public class FuncionarioDAO 
{
    static Connection conexao;
//    private static final String DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    static String sql;
    
    public FuncionarioDAO() {}
    
    public static boolean cadastrarFuncionario(Funcionario model)
    {
        boolean status = false;
        
        try 
        {
//            Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            sql = "insert into Funcionario (nome, email, tipo, status_Funcionario,cpf,telefone,DataNasc,sexo,EstadoCivil,"
                    + "DataAdm,Formacao,periodoTrabalho,Senha) "
                    + "values (?, ?, ?, ?,?,?,?,?,?,?,?,?,?)";

            PreparedStatement instrucaoSQL = conexao.prepareStatement(sql);
            
            instrucaoSQL.setString(1, model.getNome());
            instrucaoSQL.setString(2, model.getEmail());
            instrucaoSQL.setString(3, model.getTipo());
            instrucaoSQL.setString(4, model.getStatus_Funcionario());
            instrucaoSQL.setString(5, model.getCpf());
            instrucaoSQL.setString(6, model.getTelefone());
            instrucaoSQL.setString(7, model.getDataNasc());
            instrucaoSQL.setString(8, model.getSexo());
            instrucaoSQL.setString(9, model.getEstadoCivil());
            instrucaoSQL.setString(10, model.getDataAdm());
            instrucaoSQL.setString(11, model.getFormacao());
            instrucaoSQL.setString(12, model.getPeriodoTrabalho());
            instrucaoSQL.setString(13, model.getSenha());
            
            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) 
                status = true;
             
            else
              throw new Exception();
           
            conexao.close();
        } 
        catch (Exception e) 
        {
          System.out.println("Erro ao iniciar conexão com o BD");
        }
        
        return status;
    }
    
    public static boolean alterarFuncionario(Funcionario model)
    {
        boolean status = false;
        
        try 
        {
//            Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            sql = "update Funcionario set nome = ?, email = ?, tipo = ?, status_Funcionario = ? "
                   +" ,cpf=?,telefone=?,DataNasc=?,sexo=?,EstadoCivil=?,"
                    + "DataAdm=?,Formacao=?,periodoTrabalho=?,Senha=?"
                    + " where cod_Funcionario = ?";

            PreparedStatement instrucaoSQL = conexao.prepareStatement(sql);
            
            instrucaoSQL.setInt(1, model.getCod_Funcionario());
            instrucaoSQL.setString(2, model.getNome());
            instrucaoSQL.setString(3, model.getEmail());
            instrucaoSQL.setString(4, model.getTipo());
            instrucaoSQL.setString(5, model.getStatus_Funcionario());
            instrucaoSQL.setString(5, model.getCpf());
            instrucaoSQL.setString(6, model.getTelefone());
            instrucaoSQL.setString(7, model.getDataNasc());
            instrucaoSQL.setString(8, model.getSexo());
            instrucaoSQL.setString(9, model.getEstadoCivil());
            instrucaoSQL.setString(10, model.getDataAdm());
            instrucaoSQL.setString(11, model.getFormacao());
            instrucaoSQL.setString(12, model.getPeriodoTrabalho());
            instrucaoSQL.setString(13, model.getSenha());
            
            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) 
                status = true;

             else 
               throw new Exception();

            conexao.close();
        } 
        catch (Exception e) 
        {
           System.out.println("Erro ao iniciar conexão com o BD");
        }
        
        return status;
    }
    
    public static boolean removerFuncionario(int model) 
    {
        boolean status = false;
        
        try 
        {
//            Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            sql = "delete from Funcionario where cod_Funcionario = ?";

            PreparedStatement instrucaoSQL = conexao.prepareStatement(sql);
            
            instrucaoSQL.setInt(1, model);
            
            int linhasAfetadas = instrucaoSQL.executeUpdate();

            if (linhasAfetadas > 0) 
                status = true;

            else 
               throw new Exception();
            
            conexao.close();
        } 
        catch (Exception e)
        {
           System.out.println("Erro ao iniciar conexão com o BD");
        }
        
        return status;
    }
    
    public static boolean validarFuncionario(Funcionario model)
    {
        boolean status = false;
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;
        
        try 
        {
//            Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            sql = "select * from Funcionario where cod_Funcionario = ?";

            instrucaoSQL = conexao.prepareStatement(sql);
            instrucaoSQL.setInt(1, model.getCod_Funcionario());
            
            rs = instrucaoSQL.executeQuery();
    
            if (rs != null) 
            {
                rs.close();
                System.out.println("Funcionario existe!");
            }

            if (instrucaoSQL != null) {
                instrucaoSQL.close();
                System.out.println("Funcionario não existe.");
            }
            
            conexao.close();
            
        } 
        catch (Exception e) 
        {
            System.out.println("Erro na consulta.");
        }
        
        return status;
        
    }
    
    public static Funcionario consultarFuncionario(int model)
    {
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;
        Funcionario func = new Funcionario();
        try 
        {
//            Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            sql = "select * from Funcionario where cod_Funcionario = ?";
            
            instrucaoSQL = conexao.prepareStatement(sql);
            instrucaoSQL.setInt(1, model);
            
            rs = instrucaoSQL.executeQuery();
            
            if (rs.next()) 
            {
                func.setCod_Funcionario(rs.getInt("cod_Funcionario"));
                func.setNome(rs.getString("nome"));
                func.setEmail(rs.getString("email"));
                func.setTipo(rs.getString("tipo"));
                func.setStatus_Funcionario(rs.getString("status_Funcionario"));
                func.setCpf(rs.getString("cpf"));
                func.setTelefone(rs.getString("telefone"));
                func.setDataNasc(rs.getString("DataNasc"));
                func.setSexo(rs.getString("sexo"));
                func.setEstadoCivil(rs.getString("EstadoCivil"));
                func.setDataAdm(rs.getString("DataAdm"));
                func.setFormacao(rs.getString("Formacao"));
                func.setPeriodoTrabalho(rs.getString("periodoTrabalho"));
                func.setSenha(rs.getString("Senha"));
              
            }
            
        } 
        catch (Exception e) 
        {
            System.out.println("Erro na consulta");
        } 
        
        finally 
        {
            try 
            {
                if (rs != null) 
                {
                    rs.close();
                    System.out.println("Funcionario existe! Fim");
                }

                if (instrucaoSQL != null) 
                {
                    instrucaoSQL.close();
                    System.out.println("Funcionario não existe. Fim");
                }

                conexao.close();
            } 
            catch (Exception e) 
            {
                System.out.println("Falha no fechamento da conexão");
            }
            
        }
        
        return func;
    }    
    
    public static ArrayList<Funcionario> listarFuncionarios()
    {
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;

        ArrayList<Funcionario> listaFuncionario = new ArrayList<Funcionario>();
        
        try 
        {
//            Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            sql = "select cod_Funcionario,nome,email,tipo, status_Funcionario from Funcionario";

            instrucaoSQL = conexao.prepareStatement(sql);
            rs = instrucaoSQL.executeQuery();
            
            while (rs.next()) 
            {
                Funcionario prod = new Funcionario();
                
                prod.setCod_Funcionario(rs.getInt("cod_Funcionario"));
                prod.setNome(rs.getString("nome"));
                prod.setEmail(rs.getString("email"));
                prod.setTipo(rs.getString("tipo"));
                prod.setStatus_Funcionario(rs.getString("status_Funcionario"));
                prod.setCpf(rs.getString("cpf"));
                prod.setTelefone(rs.getString("telefone"));
                prod.setDataNasc(rs.getString("DataNasc"));
                prod.setSexo(rs.getString("sexo"));
                prod.setEstadoCivil(rs.getString("EstadoCivil"));
                prod.setDataAdm(rs.getString("DataAdm"));
                prod.setFormacao(rs.getString("Formacao"));
                prod.setPeriodoTrabalho(rs.getString("periodoTrabalho"));
                prod.setSenha(rs.getString("Senha"));
                
                listaFuncionario.add(prod);
            }
        } 
        catch (Exception e) 
        {
            System.out.println("Erro na listagem");
        } 
        
        finally 
        {
            try 
            {
                if (rs != null) 
                   rs.close();

                if (instrucaoSQL != null) 
                    instrucaoSQL.close();
                
                conexao.close();
            } 
            catch (Exception e) 
            {
                System.out.println("Falha no fechamento da conexão");
            }
        }
        
        return listaFuncionario;
    }
    
    public int PegarId(Funcionario model)
    {
        int cod_Funcionario = 0;
        ResultSet rs = null;
        PreparedStatement instrucaoSQL = null;
        
        try 
        {
//            Class.forName(DRIVER);
            conexao = Conexao.abrirConexao();

            sql = "select cod_Funcionario from Funcionario where cpf = ?";
            
            instrucaoSQL = conexao.prepareStatement(sql);
            instrucaoSQL.setString(1, model.getCpf());
            
            rs = instrucaoSQL.executeQuery();
            
            while (rs.next()) 
            {
               cod_Funcionario = rs.getInt("cod_Funcionario");
            }
        } 
        catch (Exception e) 
        {
            System.out.println("Erro na consulta");
        } 
        finally 
        {
            try 
            {
                if (rs != null) 
                    rs.close();

                if (instrucaoSQL != null)
                    instrucaoSQL.close();

                conexao.close();

            } 
            catch (Exception e) 
            {
                System.out.println("Falha no fechamento da conexão");
            }
        }
        
        return cod_Funcionario;
    }    
}
