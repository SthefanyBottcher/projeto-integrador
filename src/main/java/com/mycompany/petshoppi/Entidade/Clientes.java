/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshoppi.Entidade;

import java.sql.Date;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Sthefany
 */
@Getter
@Setter
public class Clientes {

    private int cod_Cliente;
    private String nome;
    private String cpf;
    private String dataNascimento;
    private String sexo;
    private String email;
    private String contato;
    private String status_Clientes;
    private String CEP;
    private String NumeroEnd;
    private String ComplementoEnd;

    public Clientes(int cod_Cliente, String nome, String cpf, String dataNascimento, String sexo, String email,
            String contato, String status_Clientes, String CEP, String NumeroEnd,String ComplementoEnd ) {
        this.cod_Cliente = cod_Cliente;
        this.nome = nome;
        this.cpf = cpf;
        this.dataNascimento = dataNascimento;
        this.sexo = sexo;
        this.email = email;
        this.contato = contato;
        this.status_Clientes = status_Clientes;
        this.CEP = CEP;
        this.NumeroEnd = NumeroEnd;
        this.ComplementoEnd = ComplementoEnd;
    }
    public Clientes(){}
    
}
