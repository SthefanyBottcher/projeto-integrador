/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshoppi.Entidade;
import java.sql.Date;
import lombok.Getter;
import lombok.Setter;
/**
 *
 * @author joaop
 */
@Getter
@Setter
public class Funcionario 
{
    private int cod_Funcionario;
    private String nome;
    private String email;
    private String tipo;
    private String status_Funcionario;
    private String cpf ;
    private String telefone ;
    private String DataNasc ;
    private String sexo ;
    private String EstadoCivil ;
    private String DataAdm;
    private String Formacao;
    private String periodoTrabalho ;
    private String Senha ;
    
    public Funcionario(int cod_Funcionario, String nome, String email, String tipo, String status_Funcionario,
           String cpf,String telefone,String DataNasc,String sexo,String EstadoCivil,String DataAdm,String Formacao,
          String periodoTrabalho,String Senha) {
        this.cod_Funcionario = cod_Funcionario;
        this.nome = nome;
        this.email = email;
        this.tipo = tipo;
        this.status_Funcionario = status_Funcionario;
        this.cpf = cpf;
        this.telefone = telefone;
        this.DataNasc = DataNasc;
        this.sexo = sexo;
        this.EstadoCivil = EstadoCivil;
        this.DataAdm = DataAdm;
        this.Formacao = Formacao;
        this.periodoTrabalho = periodoTrabalho;
        this.Senha = Senha;
    }

    public Funcionario(){}
    
    
}
