/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshoppi.Entidade;

/**
 *
 * @author joaop
 */
public class Financeiro 
{
    private int Id_Financeiro;
    private int Id_Cliente;
    private int Id_PetShop;
    private double Vl_Pendente;
    private double Vl_Pago;
    private String Observacoes;

    public Financeiro(int Id_Financeiro, int Id_Cliente, int Id_PetShop, double Vl_Pendente, double Vl_Pago, String Observacoes) {
        this.Id_Financeiro = Id_Financeiro;
        this.Id_Cliente = Id_Cliente;
        this.Id_PetShop = Id_PetShop;
        this.Vl_Pendente = Vl_Pendente;
        this.Vl_Pago = Vl_Pago;
        this.Observacoes = Observacoes;
    }

    public int getId_Financeiro() {
        return Id_Financeiro;
    }

    public void setId_Financeiro(int Id_Financeiro) {
        this.Id_Financeiro = Id_Financeiro;
    }

    public int getId_Cliente() {
        return Id_Cliente;
    }

    public void setId_Cliente(int Id_Cliente) {
        this.Id_Cliente = Id_Cliente;
    }

    public int getId_PetShop() {
        return Id_PetShop;
    }

    public void setId_PetShop(int Id_PetShop) {
        this.Id_PetShop = Id_PetShop;
    }

    public double getVl_Pendente() {
        return Vl_Pendente;
    }

    public void setVl_Pendente(double Vl_Pendente) {
        this.Vl_Pendente = Vl_Pendente;
    }

    public double getVl_Pago() {
        return Vl_Pago;
    }

    public void setVl_Pago(double Vl_Pago) {
        this.Vl_Pago = Vl_Pago;
    }

    public String getObservacoes() {
        return Observacoes;
    }

    public void setObservacoes(String Observacoes) {
        this.Observacoes = Observacoes;
    }
}
