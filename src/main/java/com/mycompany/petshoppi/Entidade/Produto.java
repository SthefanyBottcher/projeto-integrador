/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshoppi.Entidade;

import java.sql.Date;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author joaop
 */
@Getter
@Setter
public class Produto {

    private int cod_Produto;
    private String nome_Produto;
    private String tipo_Animal;
    private double valor_Venda;
    private double valor_Entrada;
    private String status_produto;
    private String codigo_barras;
    private String descricao;
    private String classificacao;
    private String data_cadastro;
    private String data_entrada;
    private int quantidade;
    private String fornecedor;

    public Produto(int cod_Produto, String nome_Produto,  String tipo_Animal, double valor_Venda, double valor_Entrada, String status_produto, String codigo_barras,
            String descricao, String classificacao, String data_cadastro, String data_entrada, int quantidade,
            String fornecedor) {
        this.cod_Produto = cod_Produto;
        this.tipo_Animal = tipo_Animal;
        this.valor_Venda = valor_Venda;
        this.valor_Entrada = valor_Entrada;
        this.status_produto = status_produto;
        this.codigo_barras = codigo_barras;
        this.descricao = descricao;
        this.classificacao = classificacao;
        this.data_cadastro = data_cadastro;
        this.data_entrada = data_entrada;
        this.quantidade = quantidade;
        this.fornecedor = fornecedor;
        this.nome_Produto = nome_Produto;
    }
    public Produto(){}
   
}
