/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshoppi.Entidade;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author joaop
 */
@Getter
@Setter
public class Login 
{
    private int Id_Login;
    private String Usuario;
    private String Senha;
    private String Tipo;
    private int Status;

    public Login(int Id_Login, String Usuario, String Senha, String Tipo, int Status) {
        this.Id_Login = Id_Login;
        this.Usuario = Usuario;
        this.Senha = Senha;
        this.Tipo = Tipo;
        this.Status = Status;
    }

}
