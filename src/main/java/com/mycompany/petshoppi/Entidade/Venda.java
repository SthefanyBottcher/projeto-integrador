/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshoppi.Entidade;

import java.sql.Date;
import java.sql.Time;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author joaop
 */
@Getter
@Setter
public class Venda {

    private int cod_venda;
    private int cod_Funcionario;
    private int cod_Cliente;
    private int cod_Produto;
    private String data_atendimento;
    private String hora_atendimento;
    private String status_venda;
    private String tipo_entrega;
    private double total_venda;
    private String cod_filial;

    public Venda(int cod_venda, int cod_Funcionario, int cod_Cliente, int cod_Produto, String data_atendimento, String hora_atendimento,
            String status_venda, String tipo_entrega, double total_venda, String cod_filial) {
        this.cod_venda = cod_venda;
        this.cod_Funcionario = cod_Funcionario;
        this.cod_Cliente = cod_Cliente;
        this.cod_Produto = cod_Produto;
        this.data_atendimento = data_atendimento;
        this.hora_atendimento = hora_atendimento;
        this.status_venda = status_venda;
        this.tipo_entrega = tipo_entrega;
        this.total_venda = total_venda;
        this.cod_filial=cod_filial;
    }

    public Venda() {
    }
    
//    public VendaItem(int cod_venda,int cod_Cliente, int cod_Funcionario,int cod_Produto, double Valor_produto){
//        this.cod_venda = cod_venda;
//        this.cod_Funcionario = cod_Funcionario;
//        this.cod_Cliente = cod_Cliente;
//        this.cod_Produto = cod_Produto;
//        this.Valor_produto = Valor_produto;
//        
//    }
//    public VendaItem(){}
}
