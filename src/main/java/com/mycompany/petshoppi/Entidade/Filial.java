/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshoppi.Entidade;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author joaop
 */
@Getter
@Setter
public class Filial 
{
    private int cod_Filial;
    private String Gerente_Filial;
    private String Filial_loc;
    private String CEP;
    private int numero;


    public  Filial(){};
    
    public  Filial(int cod_Filial, String Gerente_Filial, String Filial_loc, String CEP, int numero) {
        this.cod_Filial = cod_Filial;
        this.Gerente_Filial = Gerente_Filial;
        this.Filial_loc = Filial_loc;
        this.CEP = CEP;
        this.numero = numero;
    }

}
