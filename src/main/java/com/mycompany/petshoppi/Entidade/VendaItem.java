package com.mycompany.petshoppi.Entidade;

import java.sql.Date;
import java.sql.Time;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author dilaz
 */
@Getter
@Setter
public class VendaItem {

    private int cod_venda;
    private int cod_Funcionario;
    private int cod_Cliente;
    private int cod_Produto;
    private double total_venda;

    public VendaItem(int cod_venda, int cod_Funcionario, int cod_Cliente, int cod_Produto, double total_venda) {
        this.cod_venda = cod_venda;
        this.cod_Funcionario = cod_Funcionario;
        this.cod_Cliente = cod_Cliente;
        this.cod_Produto = cod_Produto;
        this.total_venda = total_venda;

    }

    public VendaItem() {
    }
}
