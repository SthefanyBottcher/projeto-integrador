/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshoppi.servlet;

import com.mycompany.petshoppi.Entidade.Produto;
import com.mycompany.petshoppi.dao.ProdutoDAO;
import com.mycompany.petshoppi.util.Utils;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author dilaz
 */
public class ProdutoAlterar extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String recebe = request.getParameter("cod_Produto");
        int cod_produto = Integer.parseInt(recebe);
        Produto produto = ProdutoDAO.consultarProduto(cod_produto);
 
        request.setAttribute("produto", produto);

        request.getRequestDispatcher("/Produto/CadProduto.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String nome_Produto = request.getParameter("nome");
         String descricao = request.getParameter("descricao");
        String tipo_Animal = request.getParameter("tipo_animal");
        String RecebeValorEntrada = request.getParameter("valor_entrada");
        String RecebeValorVenda = request.getParameter("valor_venda");
        double valor_Entrada = Double.valueOf(RecebeValorEntrada);
        double valor_Venda = Double.valueOf(RecebeValorVenda);
        String status_produto = request.getParameter("status");
        String classificacao = request.getParameter("classificacao");
        String codigo_barras = request.getParameter("cod_barras");
        String data_cadastro = request.getParameter("dt_cadastro");
        String data_entrada =request.getParameter("dt_cadastro");
        String RecebeQUantidade = request.getParameter("quantidade");
        int quantidade = Integer.parseInt(RecebeQUantidade);
        String fornecedor = request.getParameter("fornecedor");
         String recebe = request.getParameter("cod_Produto");
        int cod_produto = Integer.parseInt(recebe);        

        Produto produto = new Produto(cod_produto, nome_Produto, tipo_Animal, valor_Venda, valor_Entrada, status_produto,
                codigo_barras, descricao, classificacao, data_cadastro, data_entrada, quantidade, fornecedor);
        boolean ok = ProdutoDAO.alterarProduto(produto);
        Utils.RedirecionarURL(ok, response);
    }
}
