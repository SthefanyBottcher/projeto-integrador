/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshoppi.servlet;

import com.mycompany.petshoppi.Entidade.Funcionario;
import com.mycompany.petshoppi.dao.FuncionarioDAO;
import com.mycompany.petshoppi.util.Utils;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author dilaz
 */
public class FuncionarioAlterar extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String recebe=request.getParameter("cod_Funcionario");
        int cod_Funcionario = Integer.parseInt(recebe);
        Funcionario funcionario = FuncionarioDAO.consultarFuncionario(cod_Funcionario);

        request.setAttribute("Funcionario", funcionario);

        request.getRequestDispatcher("/Funcionario/CadFuncionario.jsp").forward(request, response);
    }

    private String telefone;
    private Date DataNasc;
    private String sexo;
    private String EstadoCivil;
    private Date DataAdm;
    private String Formacao;
    private String periodoTrabalho;
    private String Senha;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String sexo = "";
        String recebe=request.getParameter("cod_Funcionario");
        int cod_Funcionario = Integer.parseInt(recebe);
        String nome = request.getParameter("nome");
        String email = request.getParameter("email");
        String tipo = request.getParameter("tipo");
        String status_Funcionario = request.getParameter("status_Funcionario");
        String cpf = request.getParameter("cpf");
        String telefone = request.getParameter("telefone");
        String DataNasc = request.getParameter("DataNasc");
        if (request.getParameter("Feminino") != null) {
            sexo = "Feminino";
        } else {
            sexo = "Masculino";
        }
        String EstadoCivil = request.getParameter("EstadoCivil");
        String DataAdm = request.getParameter("DataAdm");
        String Formacao = request.getParameter("Formacao");
        String periodoTrabalho = request.getParameter("periodoTrabalho");
        String Senha = request.getParameter("Senha");

        Funcionario funcionario = new Funcionario(cod_Funcionario, nome, email, tipo, status_Funcionario, cpf,
                telefone, DataNasc, sexo, EstadoCivil, DataAdm, Formacao, periodoTrabalho, Senha);
        boolean ok = FuncionarioDAO.alterarFuncionario(funcionario);
        Utils.RedirecionarURL(ok, response);
    }
}
