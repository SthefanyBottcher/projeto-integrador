package com.mycompany.petshoppi.servlet;

import com.mycompany.petshoppi.dao.ClienteDAO;
import com.mycompany.petshoppi.util.Utils;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sthefany
 */
public class ExcluirCliente extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String cpf = request.getParameter("cpf");
        boolean ok = ClienteDAO.removerCliente(cpf);
        Utils.RedirecionarURL(ok, response);
    }
}
