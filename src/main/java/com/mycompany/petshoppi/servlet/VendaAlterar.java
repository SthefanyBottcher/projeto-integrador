/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshoppi.servlet;

import com.mycompany.petshoppi.Entidade.Venda;
import com.mycompany.petshoppi.Entidade.VendaItem;
import com.mycompany.petshoppi.dao.VendaDAO;
import com.mycompany.petshoppi.util.Utils;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author dilaz
 */
public class VendaAlterar extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String recebe = request.getParameter("cod_venda");
        int cod_venda = Integer.parseInt(recebe);
        Venda venda = VendaDAO.consultarVenda(cod_venda);
        String recebe1 = request.getParameter("cod_venda");
        int cod_vendaItem = Integer.parseInt(recebe1);
        Venda vendaItem = VendaDAO.consultarVendaItem(cod_vendaItem);
 
        request.setAttribute("venda", venda);
        request.setAttribute("venda", vendaItem);

        request.getRequestDispatcher("/Venda/CadVenda.jsp").forward(request, response);
    }

    @Override
       protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String recebe = request.getParameter("cod_venda");
        int cod_venda = Integer.parseInt(recebe);
        String recebe1 = request.getParameter("cod_Funcionario");
        int cod_Funcionario = Integer.parseInt(recebe1);
        String recebe2 = request.getParameter("cod_Cliente");
        int cod_Cliente = Integer.parseInt(recebe2);
        String recebe3 = request.getParameter("cod_Produto");
        int cod_Produto = Integer.parseInt(recebe3);
        String data_atendimento = request.getParameter("data_atendimento");
        String hora_atendimento = request.getParameter("hora_atendimento");
        String status_venda = request.getParameter("status_venda");
        String tipo_entrega = request.getParameter("tipo_entrega");
        String recebe4 = request.getParameter("total_venda");
        int total_venda = Integer.parseInt(recebe4);
        String cod_filial = request.getParameter("cod_filial"); 
        
//        for (int i = 0; lista.count>i; i++) {
//            
//        }

        Venda venda = new Venda(cod_venda, cod_Funcionario, cod_Cliente, cod_Produto, data_atendimento, hora_atendimento,
                status_venda, tipo_entrega, total_venda, cod_filial);
        boolean ok = VendaDAO.cadastrarVenda(venda);
        Utils.RedirecionarURL(ok, response);
    }
}
