/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshoppi.servlet;

import com.mycompany.petshoppi.Entidade.Produto;
import com.mycompany.petshoppi.dao.ProdutoDAO;
import com.mycompany.petshoppi.util.Utils;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author dilaz
 */
public class ProdutoServlet extends HttpServlet {
    
   @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        
        List<Produto> listProdutos = ProdutoDAO.listarProdutos();
        request.setAttribute("ListaProduto", listProdutos);
        request.getRequestDispatcher("Produto/Index.jsp").forward(request, response);
         
    }   

     @Override
     protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String nome_Produto = request.getParameter("nome");
         String descricao = request.getParameter("descricao");
        String tipo_Animal = request.getParameter("tipo_animal");
        String RecebeValorEntrada = request.getParameter("valor_entrada");
        String RecebeValorVenda = request.getParameter("valor_venda");
        double valor_Entrada = Double.valueOf(RecebeValorEntrada);
        double valor_Venda = Double.valueOf(RecebeValorVenda);
        String status_produto = "Ativo";
        String classificacao = request.getParameter("classificacao");
        String codigo_barras = request.getParameter("cod_barras");
        String data_cadastro = request.getParameter("dt_cadastro");
        String data_entrada =  request.getParameter("data_entrada");
        String RecebeQUantidade = request.getParameter("quantidade");
        int quantidade = Integer.parseInt(RecebeQUantidade);
        String fornecedor = request.getParameter("fornecedor");
        int cod_Produto = 0;
        
         Produto produto = new Produto(cod_Produto, nome_Produto, tipo_Animal, valor_Venda,valor_Entrada, status_produto,
                codigo_barras, descricao, classificacao, data_cadastro, data_entrada, quantidade, fornecedor);
         boolean ok = ProdutoDAO.cadastrarProduto(produto);
         Utils.RedirecionarURL(ok, response);
    } 
    
}