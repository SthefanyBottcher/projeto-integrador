/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshoppi.servlet;

import com.mycompany.petshoppi.dao.VendaDAO;
import com.mycompany.petshoppi.util.Utils;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author dilaz
 */
public class VendaExcluir extends HttpServlet {
@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String recebe=request.getParameter("model");
        int model = Integer.parseInt(recebe);
        boolean ok = VendaDAO.removerVenda(model);
        Utils.RedirecionarURL(ok, response);
    }
}
