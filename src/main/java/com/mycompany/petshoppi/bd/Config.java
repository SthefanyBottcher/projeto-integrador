/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshoppi.bd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sthefany
 */
public class Config {
        public static java.sql.Connection CONEXAO;
        public static String status = "Não conectado";
    static{
     try {
            Class.forName("com.mysql.jbdc.Driver");
          //  Class.forName("com.amazonaws:aws-java-sdk:jar:1.11.1017");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static Connection getConect() throws SQLException{ 
        String url = "jdbc:mysql://petshop.cdkummnyym4b.sa-east-1.rds.amazonaws.com:3306/BD_PetShop";
        //String url = "jdbc:derby://localhost:1527/BD_PetShop";
        String user = "admin";
        String password = "animefany15";
        if (CONEXAO == null) 
        {
            try 
            {
               
                CONEXAO = DriverManager.getConnection(url, user, password); 
                
                if (CONEXAO != null) 
                {
                    status = "Conexão realizada com sucesso!";
                } 
                else 
                {
                    status = "Não foi possivel realizar a conexão";
                }
            } 
            catch (SQLException e) 
            {  
              throw new SQLException("Erro ao estabelecer a conexão (Ex: login ou senha errados).");
            }
        } 
        else 
        {
            try 
            {
                if (CONEXAO.isClosed()) 
                {
                    CONEXAO = DriverManager.getConnection(url, user, password); 
                }
            } 
            catch (SQLException ex) 
            {
              throw new SQLException("Falha ao fechar a conexão.");
            }
        }
        return CONEXAO;
        
        
    }; 
    
     public static boolean fecharConexao() throws SQLException 
    {
        boolean response = false;
        try 
        {
            if (CONEXAO != null) {
                if (!CONEXAO.isClosed()) {
                    CONEXAO.close();
                }
            }
            status = "Não conectado";
            response = true;
        } 
        catch (SQLException e) 
        {
            response = false;
        }
        return response;
    }
}
