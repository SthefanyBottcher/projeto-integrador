create database BD_PetShop;
use BD_PetShop;


CREATE TABLE BD_PetShop.Funcionario (
    cod_Funcionario INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    nome VARCHAR(20) NOT NULL,
    cpf VARCHAR(20) NOT NULL,
    telefone VARCHAR(20) NOT NULL,
    DataNasc DATE NOT NULL,
    sexo char(1) NOT NULL,
    EstadoCivil VARCHAR(20) NOT NULL,
    DataAdm DATE NOT NULL,
    Formacao VARCHAR(20) NOT NULL,
    periodoTrabalho VARCHAR(20) NOT NULL,
    Senha VARCHAR(20) NOT NULL,
    email VARCHAR(30) NOT NULL,
    tipo VARCHAR(30) NOT NULL,
    status_Funcionario VARCHAR(30) NOT NULL
);


CREATE TABLE BD_PetShop.Cliente (
    cod_Cliente INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    nome VARCHAR(20) NOT NULL,
    cpf VARCHAR(11) NOT NULL UNIQUE,
    dataNascimento DATE NOT NULL,
    sexo VARCHAR(1) NOT NULL,
    email VARCHAR(30) NOT NULL,
    contato VARCHAR(11) NOT NULL,
    status_Clientes VARCHAR(30),    
    CEP VARCHAR(8) NULL,Cliente
    ComplementoEnd VARCHAR(60) NULL,
    NumeroEnd VARCHAR(60) NULL
);
CREATE TABLE BD_PetShop.Produto (
    cod_Produto INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    nome_Produto VARCHAR(30) DEFAULT NULL,
    tipo_Animal VARCHAR(30) DEFAULT NULL,
    valor_Entrada DOUBLE(9,2) DEFAULT NULL,
    valor_Venda DOUBLE(9,2) DEFAULT NULL,
    status_produto VARCHAR(30),
    codigo_barras VARCHAR(50) DEFAULT NULL,
    descricao VARCHAR(50) DEFAULT NULL,
    classificacao VARCHAR(20) DEFAULT NULL,
    data_cadastro DATE DEFAULT NULL,
    data_entrada DATE DEFAULT NULL,
    quantidade INT DEFAULT NULL,
    fornecedor VARCHAR(30)
);

CREATE TABLE Filial_PetShop (
    cod_Filial INT NOT NULL AUTO_INCREMENT,
    Gerente_Filial VARCHAR(50) DEFAULT NULL,
    Filial_loc VARCHAR(50) DEFAULT NULL,
    CEP VARCHAR(8) NULL,
    PRIMARY KEY (cod_Filial)
    
);

CREATE TABLE BD_PetShop.Venda (
    cod_venda INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    cod_Funcionario INT DEFAULT NULL,
    cod_Cliente INT DEFAULT NULL,
    cod_Filial INT DEFAULT NULL,
    data_atendimento DATE DEFAULT NULL,
    hora_atendimento TIME DEFAULT NULL,
    status_venda VARCHAR(30),
    tipo_entrega VARCHAR(30),
    total_venda DOUBLE(9 , 2 ) DEFAULT NULL,
    FOREIGN KEY (cod_Funcionario)
        REFERENCES BD_PetShop.Funcionario (cod_Funcionario),
    FOREIGN KEY (cod_Cliente)
        REFERENCES BD_PetShop.Cliente (cod_Cliente),
    FOREIGN KEY (cod_Filial)
        REFERENCES BD_PetShop.Filial_PetShop (cod_Filial)
    
);

create database BD_PetShop;
use BD_PetShop;


CREATE TABLE BD_PetShop.Funcionario (
    cod_Funcionario INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    nome VARCHAR(20) NOT NULL,
    cpf VARCHAR(20) NOT NULL,
    telefone VARCHAR(20) NOT NULL,
    DataNasc DATE NOT NULL,
    sexo char(1) NOT NULL,
    EstadoCivil VARCHAR(20) NOT NULL,
    DataAdm DATE NOT NULL,
    Formacao VARCHAR(20) NOT NULL,
    periodoTrabalho VARCHAR(20) NOT NULL,
    Senha VARCHAR(20) NOT NULL,
    email VARCHAR(30) NOT NULL,
    tipo VARCHAR(30) NOT NULL,
    status_Funcionario VARCHAR(30) NOT NULL
);


CREATE TABLE BD_PetShop.Cliente (
    cod_Cliente INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    nome VARCHAR(20) NOT NULL,
    cpf VARCHAR(11) NOT NULL UNIQUE,
    dataNascimento DATE NOT NULL,
    sexo VARCHAR(1) NOT NULL,
    email VARCHAR(30) NOT NULL,
    contato VARCHAR(11) NOT NULL,
    status_Clientes VARCHAR(30),    
    CEP VARCHAR(8) NULL,
    ComplementoEnd VARCHAR(60) NULL,
    NumeroEnd VARCHAR(60) NULL
);
CREATE TABLE BD_PetShop.Produto (
    cod_Produto INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    nome_Produto VARCHAR(30) DEFAULT NULL,
    tipo_Animal VARCHAR(30) DEFAULT NULL,
    valor_Entrada DOUBLE(9,2) DEFAULT NULL,
    valor_Venda DOUBLE(9,2) DEFAULT NULL,
    status_produto VARCHAR(30),
    codigo_barras VARCHAR(50) DEFAULT NULL,
    descricao VARCHAR(50) DEFAULT NULL,
    classificacao VARCHAR(20) DEFAULT NULL,
    data_cadastro DATE DEFAULT NULL,
    data_entrada DATE DEFAULT NULL,
    quantidade INT DEFAULT NULL,
    fornecedor VARCHAR(30)
);

CREATE TABLE Filial_PetShop (
    cod_Filial INT NOT NULL AUTO_INCREMENT,
    Gerente_Filial VARCHAR(50) DEFAULT NULL,
    Filial_loc VARCHAR(50) DEFAULT NULL,
    CEP VARCHAR(8) NULL,
    PRIMARY KEY (cod_Filial)
    
);

CREATE TABLE BD_PetShop.Venda (
    cod_venda INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    cod_Funcionario INT DEFAULT NULL,
    cod_Cliente INT DEFAULT NULL,
    cod_Filial INT DEFAULT NULL,
    cod_Produto int default null,
    data_atendimento DATE DEFAULT NULL,
    hora_atendimento TIME DEFAULT NULL,
    status_venda VARCHAR(30),
    tipo_entrega VARCHAR(30),
    total_venda DOUBLE(9 , 2 ) DEFAULT NULL,
    FOREIGN KEY (cod_Funcionario)
        REFERENCES BD_PetShop.Funcionario (cod_Funcionario),
    FOREIGN KEY (cod_Cliente)
        REFERENCES BD_PetShop.Cliente (cod_Cliente),
    FOREIGN KEY (cod_Filial)
        REFERENCES BD_PetShop.Filial_PetShop (cod_Filial),
        FOREIGN KEY (cod_Produto)
        REFERENCES BD_PetShop.Produto (cod_Produto)
    
);

INSERT INTO `BD_PetShop`.`Cliente` (`nome`, `cpf`, `dataNascimento`, `sexo`, `email`, `contato`, `status_Clientes`, `CEP`, `ComplementoEnd`, `NumeroEnd`) VALUES ('Lila Linda', '12578567845', '02-05-2007', 'f', 'email@emial.com', '4578547856', 'Ativo', '7845697', 'N/A', '2');
INSERT INTO `BD_PetShop`.`Cliente` (`nome`, `cpf`, `dataNascimento`, `sexo`, `email`, `contato`, `status_Clientes`, `CEP`, `ComplementoEnd`, `NumeroEnd`) VALUES ('Damon Salvatore', '78598535148', '02-05-1845', 'm', 'email2@email', '7565477853', 'Ativo', '024796304', 'N/A', '7');

INSERT INTO `BD_PetShop`.`Filial_PetShop` (`Gerente_Filial`, `Filial_loc`, `CEP`) VALUES ('Dalila Legal', 'Jardins', '78569025');
INSERT INTO `BD_PetShop`.`Filial_PetShop` (`Gerente_Filial`, `Filial_loc`, `CEP`) VALUES ('Stephan Salvatore', 'Santo Amaro', '78541269');

INSERT INTO `BD_PetShop`.`Funcionario` (`nome`, `cpf`, `telefone`, `DataNasc`, `sexo`, `EstadoCivil`, `DataAdm`, `Formacao`, `periodoTrabalho`, `Senha`, `email`, `tipo`, `status_Funcionario`) VALUES ('Lilinha', '78541269875', '11567852369', '06-08-1994', 'F', 'SOLTEIRA', '09-07-2019', 'Engenharia Aeroespacial', '7 horas', '1234', '4e-mail@email', 'Gerente', 'Ativo');
INSERT INTO `BD_PetShop`.`Funcionario` (`nome`, `cpf`, `telefone`, `DataNasc`, `sexo`, `EstadoCivil`, `DataAdm`, `Formacao`, `periodoTrabalho`, `Senha`, `email`, `tipo`, `status_Funcionario`) VALUES ('Helena Gilbert', '85412639874', '11547896325', '06-08-1985', 'F', 'CASADA', '09-07-2020', 'N/A', '9 horas', '4567', '9email@email', 'Recepcionista', 'Ativo');

INSERT INTO `BD_PetShop`.`Produto` (`nome_Produto`, `tipo_Animal`, `valor_Entrada`, `valor_Venda`, `status_produto`, `codigo_barras`, `descricao`, `classificacao`, `data_cadastro`, `data_entrada`, `quantidade`, `fornecedor`) VALUES ('ROYAL', 'CARACOL', '5,00', '200,00', 'ATIVO', '14514441451541034', 'INVERMECIDA', 'REMEDIO DE VERME', '2021-05-22', '2021-05-21', '1', 'Dalila´s LTDA');
INSERT INTO `BD_PetShop`.`Produto` (`nome_Produto`, `tipo_Animal`, `valor_Entrada`, `valor_Venda`, `status_produto`, `codigo_barras`, `descricao`, `classificacao`, `data_cadastro`, `data_entrada`, `quantidade`, `fornecedor`) VALUES ('ROYAL', 'CACATUA', '5000,00', '70000', 'ATIVO', '6221524184548584', 'RACAO', 'ALIMENTO', '2021-05-22', '2021-05-19', '5', 'Lorenzo LTDA');

INSERT INTO `BD_PetShop`.`Venda` (`cod_Funcionario`, `cod_Cliente`, `cod_Filial`, `cod_Produto`, `data_atendimento`, `hora_atendimento`, `status_venda`, `tipo_entrega`, `total_venda`) VALUES ('1', '1', '1', '1', '2021/05/28', '12:00', 'ATIVO', 'Deliveri', '200,00');
INSERT INTO `BD_PetShop`.`Venda` (`cod_Funcionario`, `cod_Cliente`, `cod_Filial`, `cod_Produto`, `data_atendimento`, `hora_atendimento`, `status_venda`, `tipo_entrega`, `total_venda`) VALUES ('1', '1', '1', '1', '2021/05/28', '13:00', 'ATIVO', 'Delieveri', '2,00');
