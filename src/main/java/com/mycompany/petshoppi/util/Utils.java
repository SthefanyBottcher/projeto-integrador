/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.petshoppi.util;

import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sthefany
 */
public class Utils {
    
    public static void RedirecionarURL(boolean ok, HttpServletResponse response) throws IOException{
        if(ok){
            response.sendRedirect("/projeto-integrador/Utils/ModalSucesso.jsp");
        }else{
            response.sendRedirect("/projeto-integrador/Utils/ModalError.jsp");
        }
    }
}
