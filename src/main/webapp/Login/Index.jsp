
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cliente</title>
        <c:import url="/Utils/Header.jsp"></c:import> 
    </head>

    <body>
        <div class="body">
            <c:import url="/Utils/Menu.jsp"></c:import> 
                <div class="container">
                    <h1>Tela Login</h1>
                    <br/>
                    <button type="button" class="text-center btn btn-outline-dark mb-3" onclick="window.location.href = 'Login/telaLogin.jsp'"><span class="material-icons">person_add</span> </button>

                    <table class="table">
                        <tr class="table-dark">
                            <th>Usuário</th>
                            <th>Senha</th>
                            <th></th>
                            <th></th>
                        </tr>
                    <c:forEach var="funcionario" items="${ListaFuncionario}">
                        <tr>
                            <td>${clientes.cpf}</td>
                            <td>${clientes.senha}</td>
                            <td><button type="button" class="btn btn-outline-dark" onclick="window.location.href='AlterarCliente?cpf=${funcionario.cpf}'"><span class="material-icons">edit</span></button></td>
                            <td><button type="button" class="btn btn-outline-dark" onclick="ConfimarCliente(${funcionario.cpf});"><span class="material-icons">delete</span></button></td>

                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>
    </body>
    <c:import url="/Utils/Scripts.jsp"></c:import>
</html>

