<%-- 
    Document   : Cliente
    Created on : 13/05/2021, 03:24:59
    Author     : Sthefany
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Produto</title>
        <c:import url="/Utils/Header.jsp"></c:import> 
        </head>
        <body>
            <div class="body">
            <c:import url="/Utils/Menu.jsp"></c:import> 
                <div class="container">
                    <h1>Consulta Produtos</h1>
                    <br/>
                    <button type="button" class="text-center btn btn-outline-dark mb-3" onclick="window.location.href = 'Produto/CadProduto.jsp'"><span class="material-icons">add_circle_outline</span> Novo Produto</button>

                    <table class="table">
                        <tr class="table-dark">
                            <th>Nome</th>
                            <th>Descrição</th>
                            <th>Valor</th>
                            <th></th>
                            <th></th>
                        </tr>
                    <c:forEach var="produto" items="${ListaProduto}">
                        <tr>
                            <td>${produto.nome_Produto}</td>
                            <td>${produto.descricao}</td>
                            <td>${produto.valor_Venda}</td>
                            <td><button type="button" class="btn btn-outline-dark" onclick="window.location.href = 'ProdutoAlterar?cod_Produto=${produto.cod_Produto}'"><span class="material-icons">edit</span></button></td>
                            <td><button type="button" class="btn btn-outline-dark" onclick="ConfimarProduto(${produto.cod_Produto});"><span class="material-icons">delete</span></button></td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>
    </body>
     <c:import url="/Utils/Scripts.jsp"></c:import>
</html>
