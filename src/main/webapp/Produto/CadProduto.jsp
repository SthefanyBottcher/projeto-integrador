
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:import url="/Utils/Header.jsp"></c:import>   
<body>
    <div class="body">
    <c:import url="/Utils/Menu.jsp"></c:import> 
        <div class="container">
            <h1>Produto</h1>
            <c:if test="${not empty produto}">
                <form action="ProdutoServlet" method="Post" class="row"> 
                <!--<form action="ProdutoAlterar" method="Post" class="row">--> 
                    <input type="text" class="d-none" id="cod_Produto" name="cod_Produto" value="${produto.cod_Produto}">                    
                    <div class="mb-3 col-lg-4">
                        <label for="Nome" class="form-label">Nome*:</label>
                        <input type="text" value="${produto.nome_Produto}" required class="form-control" name="nome">
                    </div>
                    <div class="mb-3 col-lg-4">
                        <label for="Nome" class="form-label">Descrição*:</label>
                        <input type="text" value="${produto.descricao}" required class="form-control" name="descricao">
                    </div>
                    <div class="mb-3 col-lg-4">
                        <label for="Nome" class="form-label">Tipo Animal*:</label>
                        <input type="text" value="${produto.tipo_Animal}" required class="form-control" name="tipo_animal">
                    </div>
                    <div class="mb-3 col-lg-4">
                        <label for="Nome" class="form-label">Valor Entrada*:</label>
                        <input type="text" value="${produto.valor_Entrada}" required class="form-control" name="valor_entrada">
                    </div>
                    <div class="mb-3 col-lg-4">
                        <label for="Nome" class="form-label">Valor Venda*:</label>
                        <input type="text" value="${produto.valor_Venda}" required class="form-control" name="valor_venda">
                    </div>
                     <div class="mb-3 col-lg-2">
                        <label for="status" class="form-label">Status</label>
                        <select class="form-select"  name="status">
                            <option value="Ativo" ${(produto.status_produto == "Ativo") ? "selected" : ""} ;>Ativo</option>
                            <option value="Cancelado" ${(produto.status_produto == "Cancelado") ? "selected" : ""} ;>Cancelado</option>
                        </select>
                    </div>    
                    <div class="mb-3 col-lg-4">
                        <label for="Nome" class="form-label">Classificação:</label>
                        <input type="text" value="${produto.classificacao}" required class="form-control" name="classificacao">
                    </div>
                    <div class="mb-3 col-lg-4">
                        <label for="Nome" class="form-label">Codigo de barras:</label>
                        <input type="text" value="${produto.codigo_barras}" required class="form-control" name="cod_barras">
                    </div>
                    <div class="mb-3 col-lg-4">
                        <label for="Nome" class="form-label">Data Cadastro*:</label>
                        <input type="text" value="${produto.data_cadastro}" data-mask="0000/00/00" required class="form-control" name="dt_cadastro">
                    </div>
                    <div class="mb-3 col-lg-4">
                        <label for="Nome" class="form-label">Data Atualização:</label>
                        <input type="text" value="${produto.data_entrada}" data-mask="0000/00/00" required class="form-control" name="dt_atualiza">
                    </div>
                    <div class="mb-3 col-lg-4">
                        <label for="Nome" class="form-label">Quantidade*:</label>
                        <input type="text" value="${produto.quantidade}" required class="form-control" name="quantidade">
                    </div>
                    <div class="mb-3 col-lg-4">
                        <label for="Nome" class="form-label">Fornecedor*:</label>
                        <input type="text" value="${produto.fornecedor}" required class="form-control" name="fornecedor">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </c:if>
            <c:if test="${empty produto}">
                <form action="/projeto-integrador/ProdutoServlet" method="POST" class="row">                     
                    <div class="mb-3 col-lg-4">
                        <label for="Nome" class="form-label">Nome*:</label>
                        <input type="text" value="" required class="form-control" name="nome">
                    </div>
                    <div class="mb-3 col-lg-4">
                        <label for="Nome" class="form-label">Descrição*:</label>
                        <input type="text" value="" required class="form-control" name="descricao">
                    </div>
                    <div class="mb-3 col-lg-4">
                        <label for="Nome" class="form-label">Tipo Animal*:</label>
                        <input type="text" value="" required class="form-control" name="tipo_animal">
                    </div>
                    <div class="mb-3 col-lg-4">
                        <label for="Nome" class="form-label">Valor Entrada*:</label>
                        <input type="number" value="" required class="form-control" name="valor_entrada">
                    </div>
                    <div class="mb-3 col-lg-4">
                        <label for="Nome" class="form-label">Valor Venda*:</label>
                        <input type="number" value="" required class="form-control" name="valor_venda">
                    </div>                    
                    <div class="mb-3 col-lg-4">
                        <label for="Nome" class="form-label">Classificação:</label>
                        <input type="text" value="" required class="form-control" name="classificacao">
                    </div>
                    <div class="mb-3 col-lg-4">
                        <label for="Nome" class="form-label">Codigo de barras:</label>
                        <input type="text" value="" required class="form-control" name="cod_barras">
                    </div>
                    <div class="mb-3 col-lg-4">
                        <label for="Nome" class="form-label">Data Cadastro*:</label>
                        <input type="text" value="" required class="form-control" data-mask="0000/00/00" name="dt_cadastro">
                    </div>
                    <div class="mb-3 col-lg-4">
                        <label for="Nome" class="form-label">Quantidade*:</label>
                        <input type="number" value="" required class="form-control" name="quantidade">
                    </div>
                    <div class="mb-3 col-lg-4">
                        <label for="Nome" class="form-label">Fornecedor*:</label>
                        <input type="text" value="" required class="form-control" name="fornecedor">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </c:if>
        </div>
    </div>
</body>